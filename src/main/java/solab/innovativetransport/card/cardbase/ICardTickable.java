package solab.innovativetransport.card.cardbase;

public interface ICardTickable {
    /**
     * 毎Tick呼び出されます。
     */
    void update();
}
